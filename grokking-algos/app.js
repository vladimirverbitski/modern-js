const my_list = [1, 3, 5, 7, 9, 12, 15, 18];

const unsorted_list = [9, 8, 7, 6, 5, 4, 3, 2, 1,];

function binary_search(list, item) {
    let low = 0;
    let high = list.length - 1;

    while (low <= high) {
        let mid = Math.round((low + high) / 2);
        let guess = list[mid];

        if (guess === item) {
            return mid;
        }

        if (guess > item) {
            high = mid - 1;
        } else {
            low = mid + 1;
        }
    }

    return null;
}

console.log(binary_search(my_list, 3));

console.log(binary_search(unsorted_list, 3));

function binary_search_q(n) {
    let q = 0;
    while (n !== 1) {
        q += 1;
        n = Math.round(n / 2);
    }
    return q;
}

// console.log(binary_search_q(512));
// console.log('------')
// console.log(binary_search_q(256));
// console.log('------')
// console.log(binary_search_q(128));

// 1.3 O(log n)

// 1.4 O(n)

// 1.5 O(n)

// 1.6 O(log n)