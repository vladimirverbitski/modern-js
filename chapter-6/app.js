// Book constructor
function Book(title, author, isbn) {
  this.title = title;
  this.author = author;
  this.isbn = isbn;
}

// UI constructor
function UI() {
  this.addBook = (book) => {
    console.log(book);
  }
}

UI.prototype.addBookToList = function(book) {
  const list = document.getElementById('book-list');
  const row = document.createElement('tr');

  row.innerHTML = `
    <td>${book.title}</td>
    <td>${book.author}</td>
    <td>${book.isbn}</td>
    <td><a href="#" class="delete">x</a></td>`;

  list.appendChild(row);
}

UI.prototype.deleteBook = (target) => {
  if (target.className === 'delete') {
    target.parentNode.parentNode.remove();
  }
}

UI.prototype.clearFields = function() {
  document.getElementById('title').value = '';
  document.getElementById('author').value = '';
  document.getElementById('isbn').value = '';
}

UI.prototype.showAlert = function(msg, className) {
  const div = document.createElement('div');
  div.className = `alert ${className}`;
  div.appendChild(document.createTextNode(msg));
  const form = document.querySelector('#book-form');
  const container = form.parentNode;
  container.insertBefore(div, form);

  setTimeout(() => {
    document.querySelector('.alert').remove();
  }, 3000);
}

// Event Listeners
document.getElementById('book-form').addEventListener('submit', function(e) {
  e.preventDefault();
  const title = document.getElementById('title').value,
        author = document.getElementById('author').value,
        isbn = document.getElementById('isbn').value;
  
  const book = new Book(title, author, isbn);

  const ui = new UI();

  if (title === '' || author === '' || isbn === '') {
    ui.showAlert('Please fill all fields.', 'error');
  } else {
    ui.addBookToList(book);
  }

  ui.clearFields();
})

document.getElementById('book-list').addEventListener('click', function(e) {
  e.preventDefault();
  const ui = new UI();

  ui.deleteBook(e.target);

  ui.showAlert('Book removed', 'success');

});