// Person constructor

function Person(firstName, lastName, dob) {
  this.firstName = firstName;
  this.lastName = lastName;
  this.birthday = new Date(dob);

  // this.getAge = () => {
  //   return Math.abs(new Date(Date.now() - this.birthday.getTime()).getUTCFullYear() - 1970);
  // }; 
}

// Prototypes

Person.prototype.getAge = function() {
  return Math.abs(new Date(Date.now() - this.birthday.getTime()).getUTCFullYear() - 1970);
};

Person.prototype.getFullName = function() {
  return `${this.firstName} ${this.lastName}`;
};

Person.prototype.getsMarried = function(newLastName) {
  this.lastName = newLastName;
};

const brad = new Person('Brad', 'Pit', '9-10-1981');
// const mary = new Person('Mary', 'Cherry', '9-10-1981');

// console.log(mary);
// console.log(mary.getAge());
// console.log(mary.getFullName());
// mary.getsMarried('Smith');
// console.log(mary.getFullName());


// Prototypal Inheritance

Person.prototype.greeting = function() {
  return `Hello there, ${this.firstName} ${this.lastName}!`;
};

const john = new Person('John', 'Doe');

function Customer(firstName, lastName, dod, phone, membership) {
  Person.call(this, firstName, lastName, dod);

  this.phone = phone;
  this.membership = membership;
}

// Inherit the person prototype methods
// Customer.prototype = Object.create(Person.prototype);

// Make customer.prototype return Customer()
Customer.prototype.constructor = Customer;

// create customer
const customer1 = new Customer('Tom', 'Smith', '9-10-1981','555-55-555', 'Standard');
console.log(customer1);

// console.log(customer1.greeting());

const personPrototypes = {
  greeting: function() {
    return `Hello there ${this.firstName} ${this.lastName}`;
  }
}

const mary = Object.create(personPrototypes);
