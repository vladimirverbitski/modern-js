// listen for submit

document.getElementById('loan-form').addEventListener('submit', function(e){
  document.getElementById('results').style.display = 'none'; 
  document.getElementById('loading').style.display = 'block';
  setTimeout(calculateResult, 5000); 
  e.preventDefault();
});

function calculateResult() {
  const amount = document.getElementById('amount');
  const interest = document.getElementById('interest');
  const years = document.getElementById('years');
  const monthlyPayemnt = document.getElementById('monthly-payment');
  const totalPayemnt = document.getElementById('total-payment');
  const totalInterest = document.getElementById('total-interest');

  const principal = parseFloat(amount.value);
  const calculatedInterest = parseFloat(interest.value) / 100 / 12;
  const calculatedPayments = parseFloat(years.value) * 12;

  const x = Math.pow(1 + calculatedInterest, calculatedPayments);
  const monthly = (principal * x * calculatedInterest) / (x - 1);

  if (isFinite(monthly)) {
    monthlyPayemnt.value = monthly.toFixed(2);
    totalPayemnt.value = (monthly * calculatedPayments).toFixed(2);
    totalInterest.value = ((monthly * calculatedPayments) - principal).toFixed(2);
    document.getElementById('results').style.display = 'block';
    document.getElementById('loading').style.display = 'none';
  } else {
    document.getElementById('loading').style.display = 'none';
    M.toast({html: 'Please, check numbers!'})
    // showError('Please, check numbers!');
  }
}

function showError(error) {
  const errorDiv = document.createElement('div');
  const card = document.querySelector('.card-content');
  const heading = document.querySelector('.card-title');

  errorDiv.className = 'alert alert-danger';
  errorDiv.appendChild(document.createTextNode(error));
  card.insertBefore(errorDiv, heading);

  setTimeout(clearError, 3000);
}

function clearError() {
  document.querySelector('.alert').remove();
}