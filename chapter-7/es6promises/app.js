const posts = [
    {title: 'Post 1'},
    {title: 'Post 2'},
    {title: 'Post 3'},
]

const newPost = {title: 'Post 4'};

function createPost(post) {
    return new Promise(function(resolve) {
        setTimeout(function() {
            posts.push(post);
            resolve();
        }, 2000)
    })
}

function getPost() {
    setTimeout(function() {
        let output = '';
        posts.forEach(function(post) {
            output += `<p>${post.title}</p>`;
        });
        console.log(output);
        document.body.innerHTMl = output;
    }, 1000);
}

createPost(newPost).then(getPost);