document.getElementById('getData').addEventListener('click', loadData);

function loadData() {
  const xhr = new XMLHttpRequest();

  xhr.open('GET', 'data.txt', true);

  xhr.onprogress = function() {
    console.log(xhr.readyState);
  }

  xhr.onload = function() {
    if (this.status === 200) {
      console.log(this.responseText);
      document.getElementById('output').innerHTML = `<p>${this.responseText}</p>`;
    }
  }

  // xhr.onreadystatechange = function() {
  //   if (this.staus === 200 && this.readyState === 4) {
  //     console.log(this.responseText);
  //   }
  // } 

  xhr.send();
}