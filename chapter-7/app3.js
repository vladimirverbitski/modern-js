document.getElementById('getJokes').addEventListener('click', getJokes);

function getJokes() {
    const jokesNumber = document.getElementById('jokesNumber').value;
    const xhr = new XMLHttpRequest();

    xhr.open('GET', `https://api.icndb.com/jokes/random/${jokesNumber}`, true);

    xhr.onload = function() {
        if (this.status === 200) {
            const parsedResponse = JSON.parse(this.responseText);
            let jokesOutput = '';

            if (parsedResponse.type === 'success') {
                parsedResponse.value.forEach((it) => {
                    jokesOutput += `
                    <div class="card horizontal">
                        <div class="card-stacked">
                            <div class="card-content"><p>${it.joke}</p></div>
                        </div>
                    </div>`;
                });
            } else {
                jokesOutput = `
                <div class="card horizontal">
                    <div class="card-stacked">
                        <div class="card-content"><p>No Jokes!</p></div>
                    </div>
                </div>`
            }

            document.getElementById('searchedJokes').innerHTML = jokesOutput;
        }
    }

    xhr.send();
}