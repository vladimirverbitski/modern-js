document.getElementById('getCustomer').addEventListener('click', getCustomerById);

document.getElementById('getAllCustomers').addEventListener('click', getAllCustomers);

function getCustomerById() {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', 'customer.json', true);

    xhr.onload = function() {
        if (this.status === 200) {
            const parsedResponse = JSON.parse(this.responseText);
            const id = getRandomNum(0, parsedResponse.length);

            document.getElementById('customer').innerHTML = `
            <ul class="collection">
                <li class="collection-item">Name: ${parsedResponse[id].first_name}</li>
                <li class="collection-item">Last name: ${parsedResponse[id].last_name}</li>
                <li class="collection-item">Email: ${parsedResponse[id].email}</li>
                <li class="collection-item">Gender: ${parsedResponse[id].gender}</li>
            </ul>
            `;
        }
    }

    xhr.send();
}

function getRandomNum(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function getAllCustomers() {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', 'customer.json', true);

    xhr.onload = function() {
        if (this.status === 200) {
            const customers = JSON.parse(this.responseText);

            const output = customers.map( c => {
                return `
                    <tr id="userRow">
                        <td>${c.id}</td>
                        <td>${c.first_name}</td>
                        <td>${c.last_name}</td>
                        <td>${c.email}</td>
                        <td>${c.gender}</td>
                    </tr>
                `;
            });

            document.getElementById('allCustomers').innerHTML = output;
        }
    }

    xhr.send();
}

function showSelectedUser(e) {
    console.log(e);
}