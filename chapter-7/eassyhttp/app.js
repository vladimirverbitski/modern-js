const http = new easyHttp;
const API_URL = 'http://jsonplaceholder.typicode.com';

// http.get(`${API_URL}/posts`, function(err, response) {
//     if (err) {
//         console.log(err);
//     } else {
//         console.log(response);
//     }
// });

const data = {
    title: 'Some new post',
    body: 'Ive posted a new post!'
}

// http.post(`${API_URL}/posts`, data, function(err, post) {
//     if (err) {
//         console.log(err);
//     } else {
//         console.log(post);
//     }
// });

// http.put(`${API_URL}/posts/1`, data, function(err, post) {
//     if (err) {
//         console.log(err);
//     } else {
//         console.log(post);
//     }
// });

http.delete(`${API_URL}/posts/100`, function(err, response) {
    if (err) {
        console.log(err);
    } else {
        console.log(response);
    }
});