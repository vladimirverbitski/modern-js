// Template Literals

const firstName = 'William';
const lastName = 'Tomson';
const age = 30;
const job = 'Web Developer';
const city = 'Miami';
let val, html;

val = `Hello, my name is ${firstName.lastIndexOf('l')}`;

// Old style (ES5)
html = '<ul><li>Name: ' + firstName + '</li><li>Age ' + age + '</li><li>Job: ' + job 
+ ' </li><li>City: ' + city + ' </li></ul>';

function hello() {
    return 'hello';
}

//New style (ES6)
html = `
    <ul>
        <li>Name: ${firstName}</li>
        <li>Age: ${age / 2}</li>
        <li>Job: ${job}</li>
        <li>City: ${city}</li>
        <li>${hello()}</li>
        <li>City: ${city}</li>
    </ul>
`;

document.body.innerHTML = html;


// Arrays & Array Methods
const numbers1 = [43, 56, 33, 23, 44, 36, 5];
const numbers2 = new Array(22, 45, 43, 76, 54);
const fruit = ['Apple', 'Banana', 'Orange', 'Pear'];
const mixed = [22, 'Hello', true, undefined, null, {a: 1, b:1}, new Date()];

let valAr;

console.log(numbers1);
console.log(valAr);


// Object Literals

const person = {
    firstName: 'Steve',
    lastName: 'Smith',
    age: 30,
    email: 'steve@aol.com',
    hobbies: ['music', 'sports'],
    address: {
        city: 'Miami',
        state: 'Florida'
    },
    getBirthYear: function() {
        return 2017 - this.age;
    }
}

let valPerson;

valPerson = person;
// Get specific value
valPerson = person.firstName;
valPerson = person['firstName'];

console.log(valPerson);

const people = [
    {id: 1, name: 'Jon', age: 30},
    {id: 2, name: 'Mike', age: 25}
]

for (let i = 0; i < people.length; i++) {
    console.log(people[i].name);
}


// Dates & Times
let valDate;

const today = new Date();
let birthday = new Date('9-10-1981 11:25:00');
birthday = new Date('September 10 1981');

valDate = birthday;

console.log(valDate);


// General loops
const ids = people.map(user => user.id);

fruit.forEach((f, index, array) => {
    // console.log(array[index]);
    // console.log(`${index} : ${f}`);
});

const user  = {
    firstName: 'John',
    lastName: 'Doe',
    age: 40
}

for (let x in user) {
    console.log(x , user[x]);
}